const { Sequelize, DataTypes } = require('sequelize')
const db = require('../../utils/db')

const product = db.define('Products', {
    productid: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    categoryid: DataTypes.STRING,
    productname: DataTypes.STRING,
    description: DataTypes.TEXT,
    price: DataTypes.DECIMAL,
    isfeatured: DataTypes.BOOLEAN,
    istopselling: DataTypes.BOOLEAN,
    isrecentlybought: DataTypes.BOOLEAN,
    image: DataTypes.JSON
}, {
    initialAutoIncrement: '100'
})

module.exports = product;