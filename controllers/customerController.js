const Customer = require('../models/Customer')
const Product = require('../models/Product')
const Cart = require('../models/Cart')
const Review = require('../models/Review')
let Category = require('../models/Category')
const apiController = require('./apiController')
const ProductQTY = require('../models/ProductQTY')
const Order = require('../models/Order')
const product = require('../models/db/Productsdb')
const { response } = require('express')



// home get route
exports.home = (req, res) => {
    if (req.session.custoemr) {
        res.render('home', {
            customer: req.session.customer,
            featuredlist: req.session.featuredlist,
            topsellinglist: req.session.topsellinglist,
            boughttogetherlist: req.session.boughttogetherlist,
            productlist: req.session.productlist,
            videolist: req.session.videolist
        });
    } else {
        res.render('home', {
            featuredlist: req.session.featuredlist,
            topsellinglist: req.session.topsellinglist,
            boughttogetherlist: req.session.boughttogetherlist,
            productlist: req.session.productlist,
            videolist: req.session.videolist
        });
    }
}

exports.customerUpdatePage = (req, res) => {
    res.render('customer-update', { customer: req.session.customer })
}


// checkout get route
exports.managecheckout = (req, res) => {
    res.render('checkout', {
        cartlist: req.session.customer.cartlist,
        customer: req.session.customer
    })
}

// product get route
exports.getProductPage = (req, res) => {
    if (req.session.productdetail != null) {
        res.render('product', {
            featuredlist: req.session.featuredlist,
            reviewlist: req.session.reviewlist,
            productdetail: req.session.productdetail,
            productqtylist: req.session.productdetail.productqtylist
        })
    } else {
        res.render('product', {
            featuredlist: req.session.featuredlist,
            reviewlist: req.session.reviewlist,
            productdetail: req.session.productdetail
        })
    }
}

// 404 get route
exports.pagenotfound = (req, res) => {
    res.render('404')
}

// signin get route
exports.signin = (req, res) => {
    res.render('signin')
}


// signup get route
exports.signup = (req, res) => {
    res.render('signup')
}


// signinwithotp get route
exports.signinwithotp = (req, res) => {
    res.render('signinwithotp')
}

// OTP validation get route
exports.validateotp = (req, res) => {
    res.render('validateotp')
}


// signup post route and it will store customer data in session
exports.customerSignup = (req, res) => {
    let customer = new Customer(req.body)
    customer.signup().then((customer) => {
        req.session.customer = {
            customerid: customer.customerid,
            name: customer.name,
            email: customer.email
        };
        req.session.save(function () {
            res.redirect('/')
        });
    }).catch((err) => {
        req.flash('errors', err)
        req.session.save(function () {
            res.redirect('/signup')
        });
    });
};

// signin post route and it will store customer data in session
exports.customerSignin = (req, res) => {
    let customer = new Customer(req.body)
    customer.login().then((customer) => {
        // console.log('customer ' + customer);
        req.session.customer = {
            customerid: customer.customerid,
            name: customer.name,
            email: customer.email
        };
        req.session.save(function () {
            res.redirect('/')
        });
    }).catch((err) => {
        req.flash('errors', err)
        console.log(err)
        req.session.save(function () {
            res.redirect("/signin");
        });
    });
};



// signout post route and it will local cookies
exports.customerSignout = (req, res) => {
    req.session.destroy(function () {
        res.redirect('/');
    });
};


// middleware for  Login check to access the page 
exports.mustBeLoggedIn = function (req, res, next) {
    if (req.session.customer) {
        next();
    } else {
        req.flash('errors', "user must be logged into access this page");
        req.session.save(function () {
            res.redirect("/signin");
        });
    }
};


// signinwithotp post route.it will send otp to customer mail and store customer email into session
exports.sendOTP = (req, res) => {
    let customer = new Customer(req.body);
    customer.sendOTP().then((record) => {
        console.log('otp send to mail')
        console.log(record)
        req.session.otp = {
            email: record.email
        }
        req.session.save(function () {
            res.redirect('/validateotp')
        });
    }).catch(error => {
        req.flash('errors', error)
        console.log(error)
        req.session.save(function () {
            res.redirect('/signinwithotp')
        })
    });
}


// validateotp post route and  it will store customer data in session
exports.customerSignWithOTP = (req, res) => {
    let otpdata = {
        email: req.session.otp.email,
        otp: req.body.validateotp
    };
    console.log(otpdata)
    let customer = new Customer(otpdata)
    customer.validateOTP().then((customer) => {
        // console.log('customer ' + customer);
        req.session.customer = {
            customerid: customer.customerid,
            name: customer.name,
            email: customer.email
        };
        req.session.save(function () {
            res.redirect('/')
        });
    }).catch((err) => {
        req.flash('errors', err)
        console.log(err)
        req.session.save(function () {
            res.redirect("/signin");
        });
    });
};



exports.customerUpdate = (req, res) => {
    let cusUpdate = new Customer(req.body);
    cusUpdate.UpdateCustomer().then((customer) => {
        console.log('customer ' + customer);
        req.session.customer = {
            customerid: customer.customerid,
            name: customer.name,
            email: customer.email,
            phone: customer.phone,
            pincode: customer.pincode,
            addressline1: customer.addressline1,
            addressline2: customer.addressline2,
            city: customer.city,
            state: customer.state
        };
        req.session.save(function () {
            res.redirect('/')
        });
        console.log(req.session.customer)
    }).catch(err => console.log(err))
}




// index page 

exports.getFeaturedList = function (req, res, next) {
    let featuredList = new Product();
    featuredList.FeaturedList().then(featuredlist => {
        if (featuredlist.length != 0) {
            req.session.featuredlist = featuredlist
        } else {
            req.session.featuredlist = null
        }
        req.session.save();
        // console.log(req.session)
        next()
    })
}



exports.getTopSellingList = function (req, res, next) {
    let topSellingList = new Product();
    topSellingList.TopSellingList().then(topsellinglist => {
        if (topsellinglist.length != 0) {
            req.session.topsellinglist = topsellinglist
        } else {
            req.session.topsellinglist = null
        }
        req.session.save();
        // console.log(req.session);
        next();
    })
}



exports.getBoughtTogetherList = function (req, res, next) {
    let topSellingList = new Product();
    topSellingList.BoughtTogetherList().then(boughttogetherlist => {
        if (boughttogetherlist.length != 0) {
            req.session.boughttogetherlist = boughttogetherlist
        } else {
            req.session.boughttogetherlist = null
        }
        req.session.save();
        // console.log(req.session);
        next();
    })
}






exports.getProductDetail = function (req, res, next) {
    let productdetail = new Product(req.params)
    productdetail.ProductDetail().then((productdetail) => {
        console.log(productdetail)
        if (productdetail != null) {
            req.session.productdetail = productdetail;
            let productqty = new ProductQTY(req.params)
            productqty.ProductQTYDetail().then(productqtylist => {
                req.session.productdetail.productqtylist = productqtylist;
                console.log(req.session.productdetail.productqtylist)
            })
        } else {
            req.session.productdetail = null;
        }
        req.session.save()
        next();
    }).catch((err) => {
        req.flash('errors', err)
        console.log(err)
        req.session.save(function () {
            res.redirect("/");
        });
    });
}






// exports.getProductDetail = function (req, res, next) {
//     let productdetail = new Product(req.params)
//     productdetail.ProductDetail().then((productdetail) => {
//         console.log(productdetail)
//         if (productdetail != null) {
//             let quantityoption = productdetail.quantityoption;
//             let option = quantityoption.split(',')
//             console.log(option)
//             productdetail.quantityoption = option;
//             console.log(productdetail)
//             // console.log('quantity option', quantityoption)
//             req.session.productdetail = productdetail;
//         } else {
//             req.session.productdetail = null;
//         }
//         req.session.save()
//         next();
//     }).catch((err) => {
//         req.flash('errors', err)
//         console.log(err)
//         req.session.save(function () {
//             res.redirect("/");
//         });
//     });
// }




exports.getProductList = function (req, res, next) {
    let product = new Product()
    product.ProductList().then((productlist) => {
        if (productlist.length != 0) {
            req.session.productlist = productlist
        } else {
            req.session.productlist = null
        }
        req.session.save()
        next()
    });
};

exports.searchProductList = function (req, res) {
    let product = new Product()
    product.ProductList().then((productlist) => {
        res.json(productlist)
    });
};




/* ---------------------------------------------Manage cart section------------------------------------- */

// cart get route

exports.manageCartPage = function (req, res) {
    res.render('cart', {
        cartlist: req.session.customer.cartlist,
        boughttogetherlist: req.session.boughttogetherlist
    });
}

exports.addCart = function (req, res) {
    // console.log(req.session.customer.customerid)
    req.body.customerid = req.session.customer.customerid
    let qtyoptval = req.body.hiddenquantityoption
    req.body.quantityoption = qtyoptval
    console.log('qtyoptval', qtyoptval)
    let cart = new Cart(req.body);
    console.log("addcart", req.body)
    cart.AddCart().then(() => {
        res.redirect('/managecart')
    }).catch((err) => {
        req.flash('errors', err)
        console.log(err)
        req.session.save(function () {
            res.redirect("/");
        });
    });
}


exports.updateCart = function (req, res, next) {
    let cart = new Cart(req.body);
    cart.UpdateCart().then((response) => {
        res.json(response)
    }).catch((err) => {
        req.flash('errors', err)
        console.log(err)
        req.session.save(function () {
            res.redirect("/");
        });
    });
}


exports.deleteCart = function (req, res) {
    let cart = new Cart(req.params);
    cart.DeleteCart().then(() => {
        req.session.customer.cart = null;
        req.session.save(() => {
            res.redirect('/managecart')
        })
    }).catch((err) => {
        req.flash('errors', err)
        console.log(err)
        req.session.save(function () {
            res.redirect("/managecart");
        });
    });
}

exports.getCartList = function (req, res, next) {
    let cart = new Cart(req.session.customer);
    cart.CartList().then((cartlist) => {
        console.log(cartlist)
        if (cartlist.length != 0) {
            req.session.customer.cartlist = cartlist;
        } else {
            req.session.customer.cartlist = null;
        }
        req.session.save()
        next()
    });
};


exports.getCheckOutCartList = function (req, res, next) {
    let cart = new Cart(req.session.customer);
    let pincode = req.session.customer.pincode;
    console.log("pincode", pincode)
    // pincode handler
    if (pincode != undefined) {

    } else {

    }
    cart.CartList().then(async (cartlist) => {
        console.log(cartlist)
        if (cartlist.length != 0) {
            req.session.customer.cartlist = cartlist;
            var finalprice = 0;
            var price = 0;
            var weight = 0;
            var quan = 0;
            var final_shipping_charge = 0;

            for (let index = 0; index < cartlist.length; index++) {
                price = parseFloat(cartlist[index].totalprice)
                weight = cartlist[index].quantityoption
                quan = parseInt(cartlist[index].quantity)
                // quantityoption handler
                if (weight.includes('Kg')) {
                    weight = parseFloat(weight) * 1000 * quan;
                }
                else if (weight.includes('L')) {
                    weight = parseFloat(weight) * 1000 * quan;
                } else {
                    weight = parseFloat(weight) * quan;
                }
                finalprice += price;
                let shippingcontrib = await apiController.ShipmentCharge(cartlist[index].productid, cartlist[index].quantityoption)
                let shippingcharge = await apiController.shippingCharge(weight, parseInt(pincode))
                shippingcontrib = shippingcontrib / 100;
                final_shipping_charge = final_shipping_charge + (shippingcharge - shippingcontrib);
                console.log("weight==============>", weight)
                console.log("final_shipping_charge==============>", final_shipping_charge)
            }

            let grand_totoal = finalprice + final_shipping_charge;
            req.session.customer.cartlist.finalprice = finalprice;
            req.session.customer.cartlist.shippingcharge = final_shipping_charge
            req.session.customer.cartlist.grandtotal = grand_totoal

            console.log("req.session.customer.cartlist", req.session.customer.cartlist)

        } else {
            req.session.customer.cartlist = null;
        }
        req.session.save()
        next()
    });
}

exports.getCartCount = function (req, res, next) {
    if (req.session.customer != undefined) {
        let cart = new Cart(req.session.customer);
        cart.CartCount().then((cartcount) => {
            console.log('cartcount', cartcount)
            req.session.customer.cartcount = cartcount;
            req.session.save()
        });
    }
    next()
};

/* ---------------------------------------------Manage review section------------------------------------- */

// review get route

exports.addReview = function (req, res) {
    req.body.customerid = req.session.customer.customerid;
    req.body.customername = req.session.customer.name;
    req.body.productid = req.session.productdetail.productid;
    let review = new Review(req.body)
    review.AddReview().then((rev) => {
        console.log(rev)
        let productid = req.session.productdetail.productid
        res.redirect('/product/detail/' + productid)
    })

    // req.flash('errors', "You did not purchased this product")
    // req.session.save(function () {
    //     let productid = req.session.productdetail.productid
    //     res.redirect('/product/detail/' + productid)
    // });

}




//  Do not delete

// exports.addReview = function (req, res) {
//     req.body.customerid = req.session.customer.customerid
//     req.body.productid = req.session.productdetail.productid
//     let review = new Review(req.body)
//     review.isVerifiedUser().then(async (response) => {
//         if (response != null) {
//             req.body.isverified = true;
//             review.AddReview().then((rev) => {
//                 let productid = req.session.productdetail.productid
//                 res.redirect('/product/detail/' + productid)
//             })
//         } else {
//             req.flash('errors', "You did not purchased this product")
//             req.session.save(function () {
//                 let productid = req.session.productdetail.productid
//                 res.redirect('/product/detail/' + productid)
//             });
//         }
//     })
// }




exports.getReviewList = function (req, res, next) {
    let review = new Review(req.params)
    review.ReviewList().then(reviewlist => {
        if (reviewlist.length != 0) {
            req.session.reviewlist = reviewlist
        } else {
            req.session.reviewlist = null;
        }
        req.session.save();
        console.log(reviewlist)
        next();
    })
}



exports.addOrder = function (req, res) {
    req.body.customerid = req.customerid;
    let cart = new Cart(req.session.customer);

    cart.CartList().then(cartlist => {
        console.log(cartlist)
        let productid = new Array(cartlist.length);
        let quantity = new Array(cartlist.length);
        let quantityoption = new Array(cartlist.length);
        for (let index = 0; index < cartlist.length; index++) {
            productid[index] = cartlist[index].productid;
            quantity[index] = cartlist[index].quantity;
            quantityoption[index] = cartlist[index].quantityoption;
        }
        req.body.productid = productid;
        req.body.quantity = quantity;
        req.body.quantityoption = quantityoption;
        let order = new Order(req.body)
        order.AddOrder().then(orderresponse => {
            apiController.payment(req.body.totalprice).then(razor_order => {
                razor_order.name = orderresponse.firstname + orderresponse.lastname;
                razor_order.email = orderresponse.email;
                razor_order.contact = orderresponse.phone;
                razor_order.product_orderid = orderresponse.orderid;
                console.log('sfgsdfgsfdgsfdgsdfg', razor_order)
                res.json(razor_order);
            }).catch((err) => {
                req.flash('errors', err)
                console.log(err);
                req.session.save(function () {
                    res.redirect("/checkout");
                });
            }).catch((err) => {
                req.flash('errors', err)
                req.session.save(function () {
                    res.redirect("/checkout");
                });
            });
        });
    }).catch((err) => {
        req.flash('errors', err)
        req.session.save(function () {
            res.redirect("/checkout");
        });
    });
}

exports.postpayment = function (req, res, next) {
    let order = new Order(req.body)
    order.UpdateOrder().then((updatedorder) => {
        let cart = new Cart(req.session.customer);
        cart.DeleteCartByCustomer().then(() => {
            console.log('updatedorder', updatedorder)
            const message = {
                to: [{
                    email: 'ganeshannt@gmail.com'
                },
                {
                    email: 'kumar@xploreindo.com'
                }],
                from: 'contact@thewildmart.com',
                subject: 'New Order! - Testing',
                //text: "I'm sending myself an email " + otp
                html: `<b><center><h1><i>Order details</i></h1> 
    <table class="">
    <thead>
    <tr>
      <td>Customer Id</td>
      <td>${updatedorder.customerid}</td>
    </tr>
    </thead>
    <thead>
    <tr>
      <td>Order Id</td>
      <td>${updatedorder.orderid}</td>
    </tr>
    </thead>
    <thead>
    <tr>
      <td>First Name</td>
      <td>${updatedorder.firstname}</td>
    </tr>
    </thead>
      <thead>
        <tr>
          <td>Last Name</td>
          <td>${updatedorder.lastname}</td>
        </tr>
        </thead>
        <thead>
        <tr>
          <td>Address line1</td>
          <td>${updatedorder.addressline1}</td>
        </tr>
        </thead>
        <thead>
        <tr>
          <td>Address Line2</td>
          <td>${updatedorder.addressline2}</td>
        </tr>
        </thead>
        <thead>
        <tr>
          <td>City</td>
          <td>${updatedorder.city}</td>
        </tr>
        </thead>
        <thead>
        <tr>
          <td>State</td>
          <td>${updatedorder.state}</td>
        </tr>
        </thead>
        <thead>
        <tr>
          <td>pincode</td>
          <td>${updatedorder.pincode}</td>
        </tr>
        </thead>
        <thead>
        <tr>
          <td>phone</td>
          <td>${updatedorder.phone}</td>
        </tr>
        </thead>
        <thead>
        <tr>
          <td>email</td>
          <td>${updatedorder.email}</td>
        </tr>
        </thead>
        <thead>
        <tr>
          <td>Razorpay OrderId</td>
          <td>${updatedorder.razorpay_order_id}</td>
        </tr>
        </thead>
        <thead>
        <tr>
          <td>Razorpay Payment Id</td>
          <td>${updatedorder.razorpay_payment_id}</td>
        </tr>
        </thead>
        <thead>
        <tr>
          <td>Product Id</td>
          <td>${updatedorder.productid}</td>
        </tr>
        </thead>
        <thead>
        <tr>
          <td>Quantity</td>
          <td>${updatedorder.quantity}</td>
        </tr>
        </thead>
        <thead>
        <tr>
          <td>Quantity Option</td>
          <td>${updatedorder.quantityoption}</td>
        </tr>
        </thead>
    </table>
    </center>    
    </b> `
            }
            apiController.sendEmail(message).then((response) => {
                if (response == '200') {
                    console.log('inside success')
                    let postpaystep = {
                        razorpay_payment_id: req.body.razorpay_payment_id,
                        razorpay_order_id: req.body.razorpay_order_id,
                        productid: updatedorder.productid,
                        quantity: updatedorder.quantity,
                        quantityoption: updatedorder.quantityoption
                    }
                    req.session.customer.postpaystep = postpaystep;
                    req.session.save(function () {
                        console.log("order done redirect");
                        next();
                    });
                } else {
                    console.log('error')
                }
            });
        });
    });
}


exports.orderDonePage = (req, res) => {
    console.log('inside final page render')
    res.render('orderdone', {
        name: req.session.customer.name,
        rpayid: req.session.customer.postpaystep.razorpay_payment_id,
        rorderid: req.session.customer.postpaystep.razorpay_order_id
    })
}



exports.orderDone = async function (req, res, next) {
    // let productqty = new ProductQTY(req.session.customer.postpaystep);
    console.log("inside order done")
    let productqtydetail = req.session.customer.postpaystep;
    console.log(productqtydetail)
    let productid = new Array();
    let quantity = new Array();
    let quantityoption = new Array();
    productid = JSON.parse(productqtydetail.productid);
    quantity = JSON.parse(productqtydetail.quantity);
    quantityoption = JSON.parse(productqtydetail.quantityoption);
    for (let index = 0; index < productid.length; index++) {
        let total_qty = await getQuantity(productid[index], quantityoption[index]);
        console.log(total_qty)
        console.log(typeof (total_qty))
        let actual_qty = parseInt(total_qty) - quantity[index];
        let productqty_update = {
            productid: productid[index],
            quantity: actual_qty,
            quantityoption: quantityoption[index]
        }
        try {
            console.log('productqty_update', productqty_update)
            let productqty = new ProductQTY(productqty_update)
            let productqty_updatecall = await productqty.DecreaseQTY();
            console.log('final page render')
            // res.json("done");
            res.redirect('/checkout/done')
        } catch (error) {
            // res.json("fail")
            res.redirect('/checkout/done')
            console.log(error)
        }
    }
}

async function getQuantity(productid, quantityoption) {
    let details = {
        productid: productid,
        quantityoption: quantityoption
    }
    try {
        let productqty = new ProductQTY(details);
        let quantity = await productqty.QuantityValue();
        return quantity;
    } catch (error) {
        console.log('Error while getting quantity value')
    }
}




exports.getQuantity = function (req, res) {
    let details = {
        productid: 100,
        quantityoption: '300g'
    }
    let productqty = new ProductQTY(details);
    productqty.QuantityValue().then((response) => {
        res.json(response);
    })
}


/* ---------------------------------------------Search and filter section------------------------------------- */


var overallfilterlist = new Array();

exports.getSearchResult = function (req, res) {
    let product = new Product(req.params)
    product.SearchResult().then((response) => {
        res.json(response)
    })
}

exports.categoryFilter = function (req, res) {
    req.params.filterarray = ['10'];
    let category = new Category(req.params)
    category.CategoryFilter().then((categoryfilter) => {
        res.json(categoryfilter)
    })
}


exports.quantityFilter = function (req, res) {
    req.params.filterarray = ['300g'];
    let productqty = new ProductQTY(req.params)
    productqty.QTYOptionFilter().then((qtyfilter) => {
        res.json(qtyfilter)
    })
}


exports.priceFilter = function (req, res) {
    req.params.priceval = 50;
    let productqty = new ProductQTY(req.params)
    productqty.QTYPriceFilter().then((pricefilter) => {
        res.json(pricefilter)
    })
}