const { Sequelize } = require('sequelize')
const mysql = require('mysql2')
require('dotenv').config()

const db = new Sequelize('thewildmart', process.env.DB_USER, process.env.DB_PWD, {
    dialect: 'mysql',
    host: process.env.HOST,
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    }
})
module.exports = db;