var minVal = 1, maxVal = 20;
$(".increaseQty").on('click', async function () {
  var $parentElm = $(this).parents(".qtySelector");
  $(this).addClass("clicked");
  setTimeout(function () {
    $(".clicked").removeClass("clicked");
  }, 100);
  var value = $parentElm.find(".qtyValue").val();
  var $rowselector = $(this).parents(".rowselector");
  var price = $rowselector.find(".totalprice").val();
  var cartid = $rowselector.find(".cartid").val();



  if (value < maxVal) {
    var initial_price = price / value;
    value++;
    price = initial_price * value;
  }

  await updatecart(cartid, value, price);

  $rowselector.find(".totalprice").val(price);
  $parentElm.find(".qtyValue").val(value);
  var finalPrice = document.getElementById('finalPrice').value = "";
  updatePrice()
  console.log("set", settings.data)
});


$(".decreaseQty").on('click', async function () {
  var $parentElm = $(this).parents(".qtySelector");
  $(this).addClass("clicked");
  setTimeout(function () {
    $(".clicked").removeClass("clicked");
  }, 100);
  var value = $parentElm.find(".qtyValue").val();
  var $rowselector = $(this).parents(".rowselector");
  var price = $rowselector.find(".totalprice").val();
  var cartid = $rowselector.find(".cartid").val();
  if (value > 1) {
    var initial_price = price / value;
    value--;
    price = initial_price * value;

  }

  await updatecart(cartid, value, price);


  $rowselector.find(".totalprice").val(price);
  $parentElm.find(".qtyValue").val(value);
  var finalPrice = document.getElementById('finalPrice').value = "";
  updatePrice()

});



function updatecart(cartid, value, price) {
  var settings = {
    "url": "http://localhost:3000/cart/updatecart",
    "method": "POST",
    "timeout": 0,
    "headers": {
      "Content-Type": "application/json",
      "Authorization": "Basic cnpwX3Rlc3RfUGcxVWlFYmJtSzF3dmg6bGlBUUROeFg0WXBWWEphNklPaFQ1alpU",
      "Cookie": "connect.sid=s%3AdmLNQUYJZfNKbybyXFyjeQ3QeWePGqgn.SH9WfMhirjn7fnhNcZnCFSzUIeEnT5pGBd7YzLypWak"
    },
    "data": JSON.stringify({
      "cartid": cartid,
      "quantity": value,
      "totalprice": price
    }),
  };

  $.ajax(settings).done(function (response) {
    console.log(response);
  });
}





function updatePrice() {
  var total_price_array = [];
  var addToCartButtons = document.getElementsByClassName('totalprice')
  for (var i = 0; i < addToCartButtons.length; i++) {
    var button = addToCartButtons[i]
    total_price_array[i] = button.value;
    console.log(total_price_array);
  }

  var sum = total_price_array.reduce(function (a, b) {
    return +a + +b;
  }, 0);
  var finalPrice = document.getElementById('finalPrice');
  finalPrice.value += sum;
}

var total_price_array = [];
var addToCartButtons = document.getElementsByClassName('totalprice')
for (var i = 0; i < addToCartButtons.length; i++) {
  var button = addToCartButtons[i]
  total_price_array[i] = button.value;
  console.log(total_price_array);
}

var sum = total_price_array.reduce(function (a, b) {
  return +a + +b;
}, 0);

var finalPrice = document.getElementById('finalPrice');
finalPrice.value += sum;