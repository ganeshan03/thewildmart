const { DataTypes } = require('sequelize')
const db = require('../../utils/db');

const video = db.define('Videos', {
    videoid: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    title: DataTypes.STRING,
    description: DataTypes.TEXT,
    link: DataTypes.STRING
})

module.exports = video;