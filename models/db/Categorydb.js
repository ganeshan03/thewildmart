const { Sequelize, DataTypes } = require('sequelize')
const db = require('../../utils/db')




const category = db.define('Categories', {
    categoryid: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name: DataTypes.STRING,
    description: DataTypes.TEXT
}, {
    initialAutoIncrement: '10'
})

module.exports = category;