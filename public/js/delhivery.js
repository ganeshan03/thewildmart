document.getElementById('pincodebtn').onclick = async function (e) {
    const pincode_backend_call = await fetch("http://localhost:3000/pincode/check/" + document.getElementById('pincode').value, {
        mode: 'cors',
        headers: {
            "Content-Type": "application/json"
        },
        method: "GET",
    }).then(async (pincode_response) => {
        var check = await pincode_response.json();
        if (check.status == 'yes') {
            alert('Delivery available')
        } else {
            alert('Invalid pincode / Delivery not available')
        }
    }).catch(err => console.log(err))
}