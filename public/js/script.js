
var carouselEI = $('.owl-carousel');
var carouselNI = $('.owl-carousel-new');


$('.featured').owlCarousel({
        loop:false,
        margin:0,
        nav:false,
        dots:true,
        autoplay:false,
        autoplayTimeout:2000,
        responsive:{
            0:{
                items:2
            },
            600:{
                items:2
            },
            1000:{
                items:4
            }
        }
    });

$('.owl-videos').owlCarousel({
        loop:false,
        margin:20,
        nav:false,
        dots:false,
        autoplay:true,
        autoplayTimeout:2000,
        responsive:{
            0:{
                items:2
            },
            600:{
                items:4
            },
            1000:{
                items:4
            }
        }
    });




$(".am-next").click(function() {
	carouselEI.trigger('next.owl.carousel');
});

$(".am-prev").click(function() {
	
	carouselEI.trigger('prev.owl.carousel');
});




