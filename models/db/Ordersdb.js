const { Sequelize, DataTypes } = require('sequelize');
const db = require('../../utils/db')

const order = db.define('Orders', {
    orderid: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        initialAutoIncrement: '10000'
    },
    customerid: DataTypes.INTEGER,
    productid: DataTypes.JSON,
    firstname: DataTypes.STRING,
    lastname: DataTypes.STRING,
    addressline1: DataTypes.STRING,
    addressline2: DataTypes.STRING,
    city: DataTypes.STRING,
    state: DataTypes.STRING,
    pincode: {
        type: DataTypes.BIGINT,
        validate: {
            len: [5, 8]
        }
    },
    email: {
        type: DataTypes.STRING,
        validate: {
            isEmail: true
        }
    },
    phone: {
        type: DataTypes.BIGINT,
        validate: {
            len: [10, 12]
        }
    },
    quantity: DataTypes.JSON,
    quantityoption: DataTypes.JSON,
    totalprice: DataTypes.DECIMAL,
    shippingprice: DataTypes.DECIMAL,
    razorpay_order_id: DataTypes.STRING,
    razorpay_payment_id: DataTypes.STRING,
    ispaymentdone: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
    }
}, {
    initialAutoIncrement: '10000'
})


module.exports = order;