const router = require('express').Router();
const customerController = require('../controllers/customerController');
const apiController = require('../controllers/apiController')
const adminController = require('../controllers/adminController');



/* ----------------------------------------- Customer related routes ----------------------------------------------------*/

// index page


router.get('/search', customerController.getSearchResult)
router.get('/filter/category', customerController.categoryFilter)
router.get('/filter/quantityoption', customerController.quantityFilter)
router.get('/filter/price', customerController.priceFilter)



router.get('/',
    customerController.getFeaturedList,
    customerController.getTopSellingList,
    customerController.getBoughtTogetherList,
    customerController.getProductList,
    customerController.getCartCount,
    adminController.getVideoList,
    customerController.home)


router.get('/search/products', customerController.searchProductList)
router.get('/customer/update', customerController.mustBeLoggedIn, customerController.customerUpdatePage)
router.post('/customer/update', customerController.mustBeLoggedIn, customerController.customerUpdate)


router.get('/product/detail/:productid',
    customerController.getFeaturedList,
    customerController.getProductDetail,
    customerController.getReviewList,
    customerController.getProductPage
)

router.post('/cart/addproduct/', customerController.mustBeLoggedIn, customerController.addCart)
router.post('/cart/updatecart/', customerController.mustBeLoggedIn, customerController.updateCart)

router.get('/managecart', customerController.mustBeLoggedIn,
    customerController.getCartList,
    customerController.getBoughtTogetherList,
    customerController.manageCartPage)

router.get('/cart/deleteproduct/:cartid', customerController.mustBeLoggedIn, customerController.deleteCart)

router.get('/checkout', customerController.mustBeLoggedIn, customerController.getCheckOutCartList, customerController.managecheckout)

router.post('/placeorder', customerController.mustBeLoggedIn, customerController.addOrder)


// authentication routes
router.get('/signin', customerController.signin)
router.post('/signin', customerController.customerSignin)
router.get('/signout', customerController.customerSignout)
router.get('/signup', customerController.signup)
router.post('/signup', customerController.customerSignup)
router.get('/signinwithotp', customerController.signinwithotp)
router.post('/signinwithotp', customerController.sendOTP)
router.get('/validateotp', customerController.validateotp)
router.post('/validateotp', customerController.customerSignWithOTP)


// review routes
router.post('/add/review', customerController.mustBeLoggedIn, customerController.addReview)






/* ----------------------------------------- 3rd party related routes ----------------------------------------------------*/


// delhivery pincode check route
router.get('/pincode/check/:pincode', apiController.checkPincode)

// razorpay route
router.post('/razorpay', apiController.payment)
router.post('/razorpay/verify', apiController.razorpayVerify)
router.post('/postpayment', customerController.mustBeLoggedIn, customerController.postpayment, customerController.orderDone)


router.get('/orderdone', customerController.mustBeLoggedIn,)
router.get('/checkout/done', customerController.mustBeLoggedIn, customerController.orderDonePage)


router.get('/getquantity', customerController.getQuantity)


/* ----------------------------------------- Admin related routes ----------------------------------------------------*/

// Admin authentication routes
router.get('/admin-signin', adminController.getAdminPage)
router.post('/admin-signin', adminController.adminSignin)
router.get('/admin-signout', adminController.adminSignout)


// Admin dashboard routes
router.get('/admin', adminController.mustBeLoggedIn, adminController.getProductList, adminController.adminIndex)



// products route
router.get('/addproduct', adminController.mustBeLoggedIn, adminController.addProductPage)
router.get('/updateproduct', adminController.mustBeLoggedIn, adminController.updateProductPage)
router.post('/addproduct', adminController.mustBeLoggedIn, adminController.getCategoryList, adminController.addProduct)
router.post('/updateproduct', adminController.mustBeLoggedIn, adminController.getCategoryList, adminController.updateProduct)
router.post('/deleteproduct', adminController.mustBeLoggedIn, adminController.deleteProduct)
router.get('/manageproduct', adminController.mustBeLoggedIn, adminController.getProductList, adminController.manageProductPage)
router.get('/getproduct/:productid', adminController.mustBeLoggedIn, adminController.getProductDetail)
router.get('/delete-product/:productid', adminController.mustBeLoggedIn, adminController.deleteProduct)


// product quantity route
router.get('/manageproductqty', adminController.mustBeLoggedIn, adminController.getProductQtylist, adminController.getProductList, adminController.manageProductQTYPage)
router.post('/addproductqty', adminController.mustBeLoggedIn, adminController.addProductQty)
router.get('/getproductqty/:productqtyid', adminController.mustBeLoggedIn, adminController.getProductList, adminController.getProductQtydetail)
router.post('/updateproductqty', adminController.mustBeLoggedIn, adminController.updateProductQty)
router.get('/deleteproductqty/:productqtyid', adminController.mustBeLoggedIn, adminController.deleteProductQty)




// Category route
router.get('/managecategory', adminController.mustBeLoggedIn, adminController.getCategoryList, adminController.manageCategoryPage)
router.post('/addcategory', adminController.mustBeLoggedIn, adminController.addCategory)
router.get('/category-update/:categoryid', adminController.mustBeLoggedIn, adminController.getCategoryDetail)
router.get('/category-delete/:categoryid', adminController.mustBeLoggedIn, adminController.deleteCategory)
router.post('/updatecategory', adminController.mustBeLoggedIn, adminController.updateCategory)


// Video route
router.get('/managevideo', adminController.mustBeLoggedIn, adminController.getVideoList, adminController.manageVideosPage)
router.post('/addvideo', adminController.mustBeLoggedIn, adminController.addVideo)
router.get('/video-update/:videoid', adminController.mustBeLoggedIn, adminController.getVideoDetail)
router.get('/video-delete/:videoid', adminController.mustBeLoggedIn, adminController.deleteVideo)
router.post('/updatevideo', adminController.mustBeLoggedIn, adminController.updateVideo)

// 404 route
router.get('*', customerController.pagenotfound)

module.exports = router