const productQTYdb = require('../models/db/ProductQTYdb')
const fetch = require('node-fetch')
const Razorpay = require('razorpay')
const nanoid = require('nanoid').nanoid
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(process.env.SENDGRID_SECRETE_KEY);
require('dotenv').config()


// creating razorpay instance
let razorpay = new Razorpay({
    key_id: process.env.RAZORPAY_KEY_ID,
    key_secret: process.env.RAZORPAY_KEY_SECRETE
})


exports.sendEmail = function (message) {
    return new Promise(async (resolve, reject) => {
        sgMail.send(message).then(() => {
            console.log('mail send')
            resolve('200')
        }).catch((error) => {
            console.log('mail not send')
            reject(error)
        })
    })
}





// creating order to do payment
exports.payment = function (amt) {
    return new Promise(async (resolve, reject) => {
        amount = parseFloat(amt) * 100;
        amount = parseInt(amount);
        var options = {
            amount: amount, // amount in the smallest currency unit
            currency: "INR",
            receipt: nanoid(8),
            payment_capture: 1
        };
        try {
            const rzp_response = await razorpay.orders.create(options)
            console.log(rzp_response);
            resolve({
                amount: rzp_response.amount,
                currency: rzp_response.currency,
                orderid: rzp_response.id,
                status: rzp_response.status
            })
        } catch (error) {
            reject(error)
        }
    })
}





// payment verification via digital signature 
exports.razorpayVerify = function (req, res) {
    body = req.body.razorpay_order_id + "|" + req.body.razorpay_payment_id;
    var crypto = require("crypto");
    var expectedSignature = crypto.createHmac('sha256', process.env.RAZORPAY_KEY_SECRETE)
        .update(body.toString())
        .digest('hex');
    console.log("sig" + req.body.razorpay_signature);
    console.log("sig" + expectedSignature);
    var response = { "status": "failure" }
    if (expectedSignature === req.body.razorpay_signature)
        response = { "status": "success" }
    res.send(response);
}





// // payment verification via digital signature 
// exports.razorpayVerify = function (razorpay_payment_id, razorpay_order_id, razorpay_signature) {
//     return new Promise((resolve, rejects) => {
//         console.log("inside verify")
//         body = razorpay_order_id + "|" + razorpay_payment_id;
//         var crypto = require("crypto");
//         var expectedSignature = crypto.createHmac('sha256', process.env.RAZORPAY_KEY_SECRETE)
//             .update(body.toString())
//             .digest('hex');
//         console.log("sig" + razorpay_signature);
//         console.log("sig" + expectedSignature);
//         var response = { "status": "failure" }
//         if (expectedSignature === razorpay_signature)
//             resolve('200')
//         rejects('400');
//     })
// }



// function to check COD availability for given pincode
exports.checkPincode = async function (req, res) {
    var pincode = req.params.pincode;
    if (req.session.customer != undefined) {
        req.session.customer.pincode = pincode
        req.session.save()
    }
    const delhivery_call = await fetch("https://track.delhivery.com/c/api/pin-codes/json/?token=" + process.env.DELHIVERY_TOKEN + "&filter_codes=" + pincode, {
        headers: {
            "Content-Type": "application/json"
        },
        method: "GET"
    }).then(async (response) => {
        var res_data = await response.json();
        console.log(res_data)
        var isAvailable = { "status": "no" }
        if (res_data.delivery_codes.length >= 1) {
            isAvailable = { "status": "yes" }
            // if (res_data.delivery_codes[0].postal_code.cod == 'Y') {
            //     isAvailable = { "status": "cod" }
            // } else {
            //     isAvailable = { "status": "nocod" }
            // }
            res.json(isAvailable)
        } else {
            res.json(isAvailable)
        }
    }).catch(err => console.log(err))
}


exports.shippingCharge = function (weight, pincode) {
    return new Promise(async (resolve, reject) => {
        var shippingCharge = 1;
        const rateCalculator_call = await fetch("https://track.delhivery.com/api/kinko/v1/invoice/charges/.json?md=S&cgm=" + weight + "&o_pin=635109&ss=Delivered&d_pin=" + pincode, {
            headers: {
                'authorization': "Token " + process.env.DELHIVERY_TOKEN,
                'accept': "application/json",
                'content-type': "application/json"
            },
            method: "GET"
        }).then(async (response) => {
            var res_data = await response.json();
            console.log("resdata", res_data)
            console.log(res_data[0].total_amount);
            shippingCharge = res_data[0].total_amount;
            resolve(shippingCharge)
        }).catch(err => reject(err));
    })
}


exports.ShipmentCharge = function (productid, quantityoption) {
    return new Promise(async (resolve, reject) => {
        productQTYdb.findOne({
            where: {
                productid: productid,
                quantityoption: quantityoption
            }
        }, {
            attributes: ['productid', 'shipmentpercent'],
            raw: true
        })
            .then(response => {
                console.log("from shipping", response.shipmentpercent)
                resolve(response.shipmentpercent)
            })
            .catch(err => {
                console.log(err)
                reject("Sorry, Problem with shipment charge calculation")
            })
    })
}