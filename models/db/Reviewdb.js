const { Sequelize, DataTypes } = require('sequelize');
const db = require('../../utils/db')

const review = db.define('Reviews', {
    reviewid: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    customerid: DataTypes.INTEGER,
    customername:DataTypes.STRING,
    productid: DataTypes.INTEGER,
    star: DataTypes.INTEGER,
    review: DataTypes.TEXT
},{
    initialAutoIncrement: '50000'
})

module.exports = review;