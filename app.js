const express = require('express');
const app = express();
const session = require('express-session');
const db = require('./utils/db');
const router = require('./utils/router');
const flash = require('connect-flash')
const dotenv = require('dotenv')
const MySQLStore = require('express-mysql-session')(session)
dotenv.config()
var path = require('path');

var dir = path.join(__dirname, 'public');

// Path setup to serve static files
app.use('/public', express.static(dir));
app.use('/product/detail/public', express.static(dir));
app.use('/managecart/public', express.static(dir));


// body parser
app.use(express.urlencoded({ extended: false }))
app.use(express.json())

// session configuration
app.use(session({
    secret: 'yahsgahskdjfbhFDFD',
    store: new MySQLStore({
        host: process.env.HOST,
        user: process.env.DB_USER,
        database: 'thewildmart',
        password: process.env.DB_PWD
    }),
    clearExpired: true,
    resave: false,
    saveUninitialized: false,
    cookie: {
        maxAge: 1000 * 60 * 60 * 24, httpOnly: true
    }
}))

// flash to show message
app.use(flash())


//  run for every request
app.use(function (req, res, next) {

    // Error and success flash message
    res.locals.errors = req.flash("errors")
    res.locals.success = req.flash("success")

    //  make current user ud available on req object

    if (req.session.customer) {
        req.customerid = req.session.customer.customerid
    } else {
        req.customerid = 0
    }

    // make user session data from within view template 
    res.locals.customer = req.session.customer
    res.locals.admin = req.session.admin
    next()
});



// template engine
// app.set('views', 'views')
app.set('view engine', 'ejs')
app.set("views", __dirname + '/views');

// Database connectivity 
db.authenticate().then(() => {
    console.log('database connected')
}).catch(err => console.log(err))

db.sync({ force: false }).then(() => console.log("synchronization completed"))
    .catch(err => console.log(err.message))


// Routes
app.use('/', router)


// Server configuration
app.listen(3000, (err) => {
    if (err) throw err;
    console.log("Server started");
})