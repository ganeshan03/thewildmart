const categorydb = require('../models/db/Categorydb')
const { Op } = require("sequelize")


let Category = function (data) {
    this.data = data;
    this.error = [];
}

Category.prototype.cleanUp = function () {
    if (typeof (this.data.categoryname) != "string") { this.data.categoryname = "" }
    if (typeof (this.data.description) != "string") { this.data.description = "" }

    this.data = {
        categoryid: this.data.categoryid,
        categoryname: this.data.categoryname.trim(),
        description: this.data.description.trim()
    }
    console.log(this.data)
}


Category.prototype.AddCategory = function () {
    return new Promise(async (resolve, reject) => {
        console.log(this.data)
        this.cleanUp()
        if (!this.error.length) {
            categorydb.create({
                name: this.data.categoryname,
                description: this.data.description
            }).then((category) => resolve()).catch(err => console.log(err))
        } else {
            reject(this.error)
        }
    })
}


Category.prototype.UpdateCategory = function () {
    return new Promise(async (resolve, reject) => {
        console.log(this.data)
        this.cleanUp()
        if (!this.error.length) {
            categorydb.update({
                name: this.data.categoryname,
                description: this.data.description
            },
                {
                    where: {
                        categoryid: this.data.categoryid
                    }
                }).then((category) => resolve(category))
                .catch(err => console.log(err))
        } else {
            reject(this.error)
        }
    })
}


Category.prototype.DeleteCategory = function () {
    return new Promise(async (resolve, reject) => {
        console.log(this.data)
        categorydb.destroy({
            where: {
                categoryid: this.data.categoryid
            },
            force: true
        }).then(() => resolve()).catch((err) => reject(err))
    })
}



Category.prototype.getCategoryList = function () {
    return new Promise(async (resolve, reject) => {
        categorydb.findAll().then(response => resolve(response))
            .catch(err => reject(err))
    })
}


Category.prototype.CategoryDetail = function () {
    return new Promise(async (resolve, reject) => {
        categorydb.findByPk(this.data.categoryid)
            .then(categorydetail => resolve(categorydetail))
            .catch(err => reject(err))
    })
}


Category.prototype.CategoryFilter= function () {
    return new Promise(async (resolve, reject) => {
        console.log(this.data.filterarray)
        categorydb.findAll({
            where: {
                categoryid: {
                    [Op.in]: this.data.filterarray
                }
            }
        })
            .then(categoryfilter => resolve(categoryfilter))
            .catch(err => reject(err))
    })
}


module.exports = Category