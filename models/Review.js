const Reviewdb = require('../models/db/Reviewdb')
const orderdb = require('../models/db/Ordersdb')

let Review = function (data) {
    this.data = data;
    this.error = [];
}

Review.prototype.cleanUp = function () {
    if (typeof (this.data.review) != "string") { this.data.review = "" }


    this.data = {
        customerid: this.data.customerid,
        productid: this.data.productid,
        customername: this.data.customername,
        star: this.data.starvalue,
        review: this.data.reviewprod.trim(),
        reviewid: this.data.reviewid
    }
    console.log("review.js", this.data)
}


Review.prototype.AddReview = function () {
    return new Promise(async (resolve, reject) => {
        // console.log(this.data)
        this.cleanUp()
        if (!this.error.length) {
            Reviewdb.create({
                customerid: this.data.customerid,
                productid: this.data.productid,
                star: this.data.star,
                customername: this.data.customername,
                review: this.data.review
            }).then((Review) => resolve()).catch(err => console.log(err))
        } else {
            reject(this.error)
        }
    })
}


Review.prototype.UpdateReview = function () {
    return new Promise(async (resolve, reject) => {
        console.log(this.data)
        this.cleanUp()
        if (!this.error.length) {
            Reviewdb.update({
                customerid: this.data.customerid,
                productid: this.data.productid,
                star: this.data.star,
                customername: this.data.customername,
                review: this.data.review
            },
                {
                    where: {
                        reviewid: this.data.reviewid
                    }
                }).then((Review) => resolve(Review))
                .catch(err => console.log(err))
        } else {
            reject(this.error)
        }
    })
}


Review.prototype.DeleteReview = function () {
    return new Promise(async (resolve, reject) => {
        console.log(this.data)
        Reviewdb.destroy({
            where: {
                reviewid: this.data.reviewid
            },
            force: true
        }).then(() => resolve()).catch((err) => reject(err))
    })
}



Review.prototype.ReviewList = function () {
    return new Promise(async (resolve, reject) => {
        Reviewdb.findAll({
            where: {
                productid: this.data.productid
            }
        }).then(response => resolve(response))
            .catch(err => reject(err))
    })
}


Review.prototype.isVerifiedUser = function () {
    return new Promise(async (resolve, reject) => {
        orderdb.findOne({
            where: {
                productid: this.data.productid,
                customerid: this.data.customerid
            }
        }).then(response => {
            resolve(response)
        }).catch(err => reject(err))
    })
}

module.exports = Review