const orderdb = require('../models/db/Ordersdb')


let Order = function (data) {
    this.data = data;
    this.error = [];
}

Order.prototype.cleanUp = function () {
    if (typeof (this.data.addressline1) != "string") { this.data.addressline1 = "" }
    if (typeof (this.data.addressline2) != "string") { this.data.addressline2 = "" }
    if (typeof (this.data.city) != "string") { this.data.city = "" }
    if (typeof (this.data.state) != "string") { this.data.state = "" }
    if (typeof (this.data.email) != "string") { this.data.email = "" }
    if (typeof (this.data.phone) != "string") { this.data.phone = "" }

    this.data = {
        orderid: this.data.orderid,
        customerid: this.data.customerid,
        productid: this.data.productid,
        firstname: this.data.firstname,
        lastname: this.data.lastname,
        quantity: this.data.quantity,
        totalprice: this.data.totalprice,
        shippingprice: this.data.shippingprice,
        addressline1: this.data.addressline1.trim(),
        addressline2: this.data.addressline2.trim(),
        city: this.data.city.trim(),
        state: this.data.state.trim(),
        pincode: this.data.pincode,
        email: this.data.email.trim(),
        phone: this.data.phone,
        quantityoption: this.data.quantityoption,
        ispaymentdone: this.data.ispaymentdone,
        razorpay_order_id: this.data.razorpay_order_id,
        razorpay_payment_id: this.data.razorpay_payment_id
    }
    console.log(this.data)
}



Order.prototype.validate = function () {

    return new Promise(async (resolve, reject) => {
        if (this.data.email == "") { this.error.push("Email should not be empty") }
        if (this.data.phone == "") { this.error.push("Phone number should not be empty") }
        if (this.data.addressline1 == "" || this.data.addressline2 == "") { this.error.push("Address should not be empty") }
        if (this.data.city == "") { this.error.push("City should not be empty") }
        if (this.data.state == "") { this.error.push("State should not be empty") }
        if (this.data.pincode == "") { this.error.push("Pincode should not be empty") }
        if (parseInt(this.data.phone.length) < 10 || parseInt(this.data.phone.length) > 12) { this.error.push("Enter valid phone number") }
        resolve()
    })
}






Order.prototype.AddOrder = function () {
    return new Promise(async (resolve, reject) => {
        console.log("inside order", this.data)
        this.cleanUp()
        await this.validate()
        if (!this.error.length) {
            orderdb.create({
                customerid: this.data.customerid,
                productid: this.data.productid,
                firstname: this.data.firstname,
                lastname: this.data.lastname,
                quantity: this.data.quantity,
                totalprice: this.data.totalprice,
                shippingprice: this.data.shippingprice,
                addressline1: this.data.addressline1.trim(),
                addressline2: this.data.addressline2.trim(),
                city: this.data.city.trim(),
                state: this.data.state.trim(),
                pincode: this.data.pincode,
                email: this.data.email.trim(),
                phone: this.data.phone,
                quantityoption: this.data.quantityoption,
                razorpay_id: this.data.razorpay_id,
                ispaymentdone: this.data.ispaymentdone
            }).then((Order) => resolve(Order)).catch(err => reject("problem with order placement"))
        } else {
            reject(this.error)
        }
    })
}


Order.prototype.UpdateOrder = function () {
    return new Promise(async (resolve, reject) => {
        console.log(this.data)
        orderdb.update({
            razorpay_order_id: this.data.razorpay_order_id,
            razorpay_payment_id: this.data.razorpay_payment_id,
            ispaymentdone: true
        },
            {
                where: {
                    orderid: this.data.orderid
                }
            }).then((Order) => {
                orderdb.findOne({
                    where: {
                        orderid: this.data.orderid
                    }
                }).then(updatedorderdata => {
                    console.log(updatedorderdata);
                    resolve(updatedorderdata)
                })
            })
            .catch(err => reject(err))
    })
}


Order.prototype.DeleteOrder = function () {
    return new Promise(async (resolve, reject) => {
        console.log(this.data)
        orderdb.destroy({
            where: {
                orderid: this.data.orderid
            },
            force: true
        }).then(() => resolve()).catch((err) => reject(err))
    })
}



Order.prototype.getOrderList = function () {
    return new Promise(async (resolve, reject) => {
        orderdb.findAll().then(response => resolve(response))
            .catch(err => reject(err))
    })
}


Order.prototype.OrderDetail = function () {
    return new Promise(async (resolve, reject) => {
        orderdb.findByPk(this.data.orderid)
            .then(orderdetail => resolve(orderdetail))
            .catch(err => reject(err))
    })
}

module.exports = Order