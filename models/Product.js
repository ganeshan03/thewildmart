const productdb = require('../models/db/Productsdb')
const { QueryTypes,sequelize } = require('sequelize');
const db = require('../utils/db')


let Product = function (data) {
    this.data = data;
    this.error = [];
}

Product.prototype.cleanUp = function () {
    console.log(this.data)
    if (typeof (this.data.productname) != "string") { this.data.productname = "" }
    if (typeof (this.data.description) != "string") { this.data.description = "" }
    if (typeof (this.data.quantity) != "string") { this.data.quantity = "" }
    if (typeof (this.data.quantitytype) != "string") { this.data.quantitytype = "" }
    if (typeof (this.data.quantityoption) != "string") { this.data.quantityoption = "" }
    if (typeof (this.data.categoryid) != "string") { this.data.categoryid = "" }


    this.data = {
        productid: this.data.productid,
        productname: this.data.productname.trim(),
        description: this.data.description.trim(),
        price: this.data.price,
        categoryid: this.data.categoryid,
        isfeatured: this.data.isfeatured,
        istopselling: this.data.istopselling,
        isrecentlybought: this.data.isrecentlybought,
        image: this.data.image
    }
    console.log(this.data)
}




Product.prototype.AddProduct = function () {
    return new Promise(async (resolve, reject) => {
        console.log(this.data)
        this.cleanUp()

        if (!this.error.length) {
            productdb.create({
                productname: this.data.productname,
                description: this.data.description,
                price: this.data.price,
                categoryid: this.data.categoryid,
                isfeatured: this.data.isfeatured,
                istopselling: this.data.istopselling,
                isrecentlybought: this.data.isrecentlybought,
                image: this.data.image
            }).then((product) => resolve(product)).catch(err => console.log(err))
        } else {
            reject(this.error)
        }
    })
}




Product.prototype.UpdateProduct = function () {
    return new Promise(async (resolve, reject) => {
        console.log(this.data)
        this.cleanUp()
        productdb.update({
            productname: this.data.productname,
            description: this.data.description,
            price: this.data.price,
            categoryid: this.data.categoryid,
            isfeatured: this.data.isfeatured,
            istopselling: this.data.istopselling,
            isrecentlybought: this.data.isrecentlybought,
            image: this.data.image
        },
            {
                where: {
                    productid: this.data.productid
                }
            }).then(output => resolve(output))
            .catch(err => reject(err))
    })
}




Product.prototype.DeleteProduct = function () {
    return new Promise(async (resolve, reject) => {
        console.log(this.data)
        productdb.destroy({
            where: {
                productid: this.data.productid
            },
            force: true
        }).then(() => resolve()).catch((err) => reject(err))
    })
}


Product.prototype.ProductList = function () {
    return new Promise(async (resolve, reject) => {
        productdb.findAll({
            attributes: ['productid', 'categoryid', 'productname', 'description', 'image', 'price'],
            raw: true
        })
            .then(response => {
                response.forEach(msg => console.log(msg.productid))
                resolve(response)
            })
            .catch(err => reject(err))
    })
}


Product.prototype.ProductDetail = function () {
    return new Promise(async (resolve, reject) => {
        productdb.findByPk(this.data.productid, {
            attributes: ['productid', 'categoryid', 'productname', 'description', 'image', 'price', 'isfeatured', 'istopselling', 'isrecentlybought'],
            raw: true
        })
            .then(response => {
                resolve(response)
            })
            .catch(err => reject(err))
    })
}


Product.prototype.FeaturedList = function () {
    return new Promise(async (resolve, reject) => {
        productdb.findAll({
            where: {
                isfeatured: 1
            },
            attributes: ['productid', 'productname', 'image', 'price'],
            raw: true
        }).then(featurelist => {
            // console.log('ganeshan', featurelist)
            resolve(featurelist)
        })
            .catch(err => reject(err))
    })
}




Product.prototype.TopSellingList = function () {
    return new Promise(async (resolve, reject) => {
        productdb.findAll({
            where: {
                istopselling: 1
            },
            attributes: ['productid', 'productname', 'image', 'price'],
            raw: true
        }).then(topsellinglist => {
            // console.log('ganeshan', topsellinglist)
            resolve(topsellinglist)
        })
            .catch(err => reject(err))
    })
}




Product.prototype.BoughtTogetherList = function () {
    return new Promise(async (resolve, reject) => {
        productdb.findAll({
            where: {
                isrecentlybought: 1
            },
            attributes: ['productid', 'productname', 'image', 'price'],
            raw: true
        }).then(boughttogetherlist => {
            // console.log('ganeshan', boughttogetherlist)
            resolve(boughttogetherlist)
        })
            .catch(err => reject(err))
    })
}



Product.prototype.SearchResult = function () {
    return new Promise(async (resolve, reject) => {

        let result = await productdb.sequelize.query(
            'SELECT * FROM products WHERE products.productname LIKE "%good%" OR products.description LIKE "%good%"',{ type: QueryTypes.SELECT }
          );
          console.log("search result",result)
          resolve(result)
    })
}



module.exports = Product