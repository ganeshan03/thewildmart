const productQTYdb = require('./db/ProductQTYdb')
const { Op } = require("sequelize")


let ProductQTY = function (data) {
    this.data = data;
    this.error = [];
}

ProductQTY.prototype.cleanUp = function () {
    console.log(this.data)
    if (typeof (this.data.quantity) != "string") { this.data.quantity = "" }
    if (typeof (this.data.quantityoption) != "string") { this.data.quantityoption = "" }


    this.data = {
        productqtyid: this.data.productqtyid,
        productid: this.data.productid,
        price: this.data.price,
        quantity: this.data.quantity,
        quantityoption: this.data.quantityoption,
        shipmentpercent: this.data.shipmentpercent,
    }
    console.log(this.data)
}




ProductQTY.prototype.AddProductQTY = function () {
    return new Promise(async (resolve, reject) => {
        console.log(this.data)
        this.cleanUp()
        if (!this.error.length) {
            productQTYdb.create({
                productid: this.data.productid,
                price: this.data.price,
                quantity: this.data.quantity,
                quantityoption: this.data.quantityoption,
                shipmentpercent: this.data.shipmentpercent,
            }).then((product) => resolve(product)).catch(err => console.log(err))
        } else {
            reject("Sorry, Product quantity isn't added")
        }
    })
}




ProductQTY.prototype.UpdateProductQTY = function () {
    return new Promise(async (resolve, reject) => {
        console.log(this.data)
        this.cleanUp()
        productQTYdb.update({
            productid: this.data.productid,
            price: this.data.price,
            quantity: this.data.quantity,
            quantityoption: this.data.quantityoption,
            shipmentpercent: this.data.shipmentpercent,
        },
            {
                where: {
                    productqtyid: this.data.productqtyid
                }
            }).then(output => resolve(output))
            .catch(err => reject("Sorry, Product quantity isn't updated"))
    })
}





ProductQTY.prototype.DecreaseQTY = function () {
    return new Promise(async (resolve, reject) => {
        console.log('data ', this.data)
        productQTYdb.update({
            quantity: this.data.quantity,
        },
            {
                where: {
                    productid: this.data.productid,
                    quantityoption: this.data.quantityoption
                }
            }).then(output => resolve(output))
            .catch(err => reject(err))
    })
}


ProductQTY.prototype.QuantityValue = function () {
    return new Promise(async (resolve, reject) => {
        console.log('quantity value function', this.data);
        productQTYdb.findOne(
            {
                where: {
                    quantityoption: this.data.quantityoption,
                    productid: this.data.productid
                }
            })
            .then(response => {
                resolve(response.quantity)
            })
            .catch(err => reject(err))
    })
}



ProductQTY.prototype.DeleteProductQTY = function () {
    return new Promise(async (resolve, reject) => {
        console.log(this.data)
        productQTYdb.destroy({
            where: {
                productqtyid: this.data.productqtyid
            },
            force: true
        }).then(() => resolve()).catch((err) => reject("Sorry, Delete not performed"))
    })
}


ProductQTY.prototype.ProductQTYList = function () {
    return new Promise(async (resolve, reject) => {
        productQTYdb.findAll({
            attributes: ['productqtyid', 'productid', 'quantityoption', 'quantity', 'price', 'shipmentpercent'],
            raw: true
        })
            .then(response => {
                response.forEach(msg => console.log(msg.productid))
                resolve(response)
            })
            .catch(err => reject("Sorry, Product quantity list not loaded / List is empty"))
    })
}



ProductQTY.prototype.ProductQTYDetail = function () {
    return new Promise(async (resolve, reject) => {
        productQTYdb.findAll({
            where: {
                productid: this.data.productid
            }
        }, {
            attributes: ['productqtyid', 'productid', 'quantityoption', 'quantity', 'price', 'shipmentpercent'],
            raw: true
        })
            .then(response => {
                console.log("from db", response)
                resolve(response)
            })
            .catch(err => reject("Sorry, Product quantity list not loaded / List is empty"))
    })
}


ProductQTY.prototype.QTYOptionFilter = function () {
    return new Promise(async (resolve, reject) => {
        console.log(this.data.filterarray)
        productQTYdb.findAll({
            where: {
                quantityoption: {
                    [Op.in]: this.data.filterarray
                }
            }
        })
            .then(qtyfilter => resolve(qtyfilter))
            .catch(err => reject(err))
    })
}



ProductQTY.prototype.QTYPriceFilter = function () {
    return new Promise(async (resolve, reject) => {
        console.log(this.data.priceval)
        productQTYdb.findAll({
            where: {
                price: {
                    [Op.lt]: this.data.priceval
                }
            }
        })
            .then(qtyfilter => resolve(qtyfilter))
            .catch(err => reject(err))
    })
}









module.exports = ProductQTY