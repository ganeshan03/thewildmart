const { Sequelize, DataTypes } = require('sequelize')
const db = require('../../utils/db')

const productqty = db.define('ProductQuantities', {
    productqtyid: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    productid: DataTypes.INTEGER,
    quantity: DataTypes.DECIMAL,
    quantityoption: DataTypes.STRING,
    price: DataTypes.DECIMAL,
    shipmentpercent: DataTypes.INTEGER
},{
    initialAutoIncrement: '1000'
})

module.exports = productqty;