const cartdb = require('../models/db/Cartdb')


let Cart = function (data) {
    this.data = data;
    this.error = [];
}

Cart.prototype.cleanUp = function () {
    if (typeof (this.data.customerid) != "number") { this.data.customerid = "" }
    if (typeof (this.data.productid) != "string") { this.data.productid = "" }
    if (typeof (this.data.quantityoption) != "string") { this.data.quantityoption = "" }
    if (typeof (this.data.imageurl) != "string") { this.data.imageurl = "" }

    this.data = {
        cartid: this.data.cartid,
        customerid: this.data.customerid,
        productid: this.data.productid,
        productname: this.data.productname,
        imageurl: this.data.imageurl,
        quantity: this.data.quantity,
        quantityoption: this.data.quantityoption,
        totalprice: this.data.totalprice
    }
    console.log(this.data)
}


Cart.prototype.AddCart = function () {
    return new Promise(async (resolve, reject) => {
        // console.log(this.data)
        this.cleanUp()
        if (!this.error.length) {
            cartdb.create({
                customerid: this.data.customerid,
                productid: this.data.productid,
                productname: this.data.productname,
                imageurl: this.data.imageurl,
                quantity: this.data.quantity,
                quantityoption: this.data.quantityoption,
                totalprice: this.data.totalprice
            }).then((cart) => resolve()).catch(err => console.log(err))
        } else {
            reject(this.error)
        }
    })
}


Cart.prototype.UpdateCart = function () {
    return new Promise(async (resolve, reject) => {
        console.log(this.data)
        this.cleanUp()
        if (!this.error.length) {
            cartdb.update({
                quantity: this.data.quantity,
                totalprice: this.data.totalprice
            },
                {
                    where: {
                        cartid: this.data.cartid
                    }
                }).then((cart) => resolve(cart))
                .catch(err => console.log(err))
        } else {
            reject(this.error)
        }
    })
}


Cart.prototype.DeleteCart = function () {
    return new Promise(async (resolve, reject) => {
        console.log(this.data)
        cartdb.destroy({
            where: {
                cartid: this.data.cartid
            },
            force: true
        }).then(() => resolve()).catch((err) => reject(err))
    })
}


Cart.prototype.DeleteCartByCustomer = function () {
    return new Promise(async (resolve, reject) => {
        console.log(this.data)
        cartdb.destroy({
            where: {
                customerid: this.data.customerid
            },
            force: true
        }).then(() => resolve()).catch((err) => reject(err))
    })
}






Cart.prototype.CartList = function () {
    return new Promise(async (resolve, reject) => {
        cartdb.findAll({
            where: {
                customerid: this.data.customerid
            }
        }).then(response => resolve(response))
            .catch(err => reject(err))
    })
}


Cart.prototype.CartCount = function () {
    return new Promise(async (resolve, reject) => {
        if (this.data != null) {
            cartdb.count({
                where: {
                    customerid: this.data.customerid
                }
            }).then(response => resolve(response))
                .catch(err => reject(err))
        } else {
            resolve(0);
        }
    })
}

module.exports = Cart