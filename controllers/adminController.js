const Product = require('../models/Product')
const Category = require('../models/Category')
const Video = require('../models/Video')
const Admin = require('../models/Admin')
const ProductQTY = require('../models/ProductQTY')
const fs = require("fs")
const { exception } = require('console')
// const { type } = require('os')



/* ---------------------------------------------Admin Dashboard section------------------------------------- */


exports.adminIndex = function (req, res) {
    if (req.session.admin) {
        res.render('admin-index', { admin: req.session.admin });
    } else {
        res.render('admin-signin');
    }
}


/* ---------------------------------------------Admin authentication section------------------------------------- */


exports.addProductPage = function (req, res) {
    res.render('admin-addproduct', {
        categorylist: req.session.categorylist
    })
}



exports.updateProductPage = function (req, res) {
    res.render('admin-updateproduct', {
        productdetail: req.session.admin.productdetail,
        categorylist: req.session.categorylist
    })
}


exports.manageProductPage = function (req, res) {
    // console.log(req.session)
    if (req.session.admin) {
        console.log(req.session.admin.product)
        res.render('admin-manageproducts', { productlist: req.session.productlist })
    } else {
        res.render('admin-manageproducts')
    }
}









exports.addProduct = function (req, res) {
    const multer = require('multer')
    var path = "public/image";
    var storage = multer.diskStorage({
        destination: function (req, file, cb) {
            console.log("storeage ", file)
            if (!fs.existsSync(path)) {
                fs.mkdirSync(path)
            }
            cb(null, path)
        },
        filename: function (req, file, cb) {
            let extArray = file.mimetype.split("/");
            let extension = extArray[extArray.length - 1];
            cb(null, file.fieldname + '-' + Date.now() + '.' + extension)
        }
    })

    var upload = multer({ storage: storage }).array('image', 5)

    upload(req, res, function (err) {
        if (err) {
            console.log('problem with upload');
        } else {
            console.log(req.files)
            let arrayimage = req.files;
            let imageurls = [];
            for (let i = 0; i < arrayimage.length; i++) {
                imageurls[i] = arrayimage[i].path
            }
            // store image as a JSON in mysql
            let image = {
                "imagearray": imageurls
            }

            const imagejson = JSON.stringify(imageurls);
            // get back to string to access image files
            const image_data = JSON.parse(imagejson);
            image_data.forEach((imagelink) => {
                console.log(imagelink)
            })

            req.body.image = image;

            let pricelistbody = req.body.price;



            let product = new Product(req.body);

            product.AddProduct().then((response) => {
                res.redirect('/manageproduct')
            }).catch(err => {
                req.flash('errors', err)
                console.log(err)
                req.session.save(function () {
                    res.redirect("/admin");
                });
            });
        }
    });
}


exports.updateProduct = function (req, res) {
    const multer = require('multer')
    var path = "public/image";
    var storage = multer.diskStorage({
        destination: function (req, file, cb) {
            console.log("storeage ", file)
            if (!fs.existsSync(path)) {
                fs.mkdirSync(path)
            }
            cb(null, path)
        },
        filename: function (req, file, cb) {
            let extArray = file.mimetype.split("/");
            let extension = extArray[extArray.length - 1];
            cb(null, file.fieldname + '-' + Date.now() + '.' + extension)
        }
    })

    var upload = multer({ storage: storage }).array('image', 5)

    upload(req, res, function (err) {
        if (err) {
            console.log('problem with upload');
        } else {
            console.log(req.files)
            let arrayimage = req.files;
            let imageurls = [];
            for (let i = 0; i < arrayimage.length; i++) {
                imageurls[i] = arrayimage[i].path
            }
            // store image as a JSON in mysql
            let image = {
                "imagearray": imageurls
            }

            const imagejson = JSON.stringify(imageurls);
            // get back to string to access image files
            const image_data = JSON.parse(imagejson);
            image_data.forEach((imagelink) => {
                console.log(imagelink)
            })

            req.body.image = image;
            let product = new Product(req.body);

            product.UpdateProduct().then(() => {
                console.log("Product updated")
                req.session.admin.productdetail = null;
                req.session.save(() => {
                    res.redirect('/manageproduct')
                })
            }).catch(err => {
                req.flash('errors', err)
                console.log(err)
                req.session.save(function () {
                    res.redirect("/addproduct");
                });
            });
        }
    });
}



exports.deleteProduct = function (req, res) {
    let product = new Product(req.params);
    product.DeleteProduct().then((response) => {
        console.log(response)
        req.session.admin.productdetail = null;
        req.session.save(() => {
            res.redirect('/manageproduct')
        })
    }).catch((err) => {
        req.flash('errors', err)
        console.log(err)
        req.session.save(function () {
            res.redirect("/admin");
        });
    });
}


exports.getProductDetail = function (req, res, next) {
    let productdetail = new Product(req.params)
    productdetail.ProductDetail().then((productdetail) => {
        req.session.admin.productdetail = productdetail;
        req.session.save(() => {
            res.redirect('/updateproduct')
        })
        console.log(req.session.admin)
    }).catch((err) => {
        req.flash('errors', err)
        console.log(err)
        req.session.save(function () {
            res.redirect("/admin");
        });
    });
}




exports.getProductList = function (req, res, next) {
    let product = new Product()
    product.ProductList().then((productlist) => {
        // console.log(productlist[0])
        if (productlist.length != 0) {
            req.session.productlist = productlist;

        } else {
            req.session.productlist = null;
        }
        req.session.save()
        next()
    });
};


/* ---------------------------------------------Manage product quantity section------------------------------------- */



exports.manageProductQTYPage = function (req, res) {
    res.render('admin-manageproductqty', {
        productqtylist: req.session.productqtylist,
        productqtydetail: req.session.admin.productqtydetail,
        productlist: req.session.productlist
    })
}



exports.addProductQty = function (req, res) {
    let productqty = new ProductQTY(req.body)
    productqty.AddProductQTY().then(response => {
        res.redirect('/manageproductqty')
    }).catch(error => {
        req.flash('errors', error)
        req.session.save(function () {
            res.redirect('/manageproductqty');
        });
    })
}


exports.updateProductQty = function (req, res) {
    let productqty = new ProductQTY(req.body)
    productqty.UpdateProductQTY().then(response => {
        req.session.admin.productqtydetail = null;
        res.redirect('/manageproductqty')
    }).catch(error => {
        req.flash('errors', error)
        console.log(err)
        req.session.save(function () {
            res.redirect('/manageproductqty');
        });
    })
}




exports.deleteProductQty = function (req, res) {
    let productqty = new ProductQTY(req.params)
    productqty.DeleteProductQTY().then(response => {
        res.redirect('/manageproductqty')
    }).catch(error => {
        req.flash('errors', error)
        req.session.save(function () {
            res.redirect("/admin");
        });
    })
}




exports.getProductQtylist = function (req, res, next) {
    let productqty = new ProductQTY()
    productqty.ProductQTYList().then(productqtylist => {
        console.log(productqtylist[0])
        if (productqtylist.length != 0) {
            req.session.productqtylist = productqtylist;
        } else {
            req.session.productqtylist = null;
        }
    }).catch(error => {
        req.flash('errors', error);
    })
    req.session.save();
    next()
}


exports.getProductQtydetail = function (req, res, next) {
    let productqty = new ProductQTY(req.params)
    productqty.ProductQTYDetail().then(productqtydetail => {
        req.session.admin.productqtydetail = productqtydetail;
    }).catch(error => {
        req.flash('errors', error)
    })
    req.session.save(function () {
        res.redirect('/manageproductqty');
    })
    // next()
}









/* ---------------------------------------------Manage category section------------------------------------- */


exports.manageCategoryPage = function (req, res) {
    res.render('admin-managecategory', {
        categorylist: req.session.categorylist,
        categorydetail: req.session.admin.categorydetail
    });
}

exports.addCategory = function (req, res) {
    let category = new Category(req.body);
    category.AddCategory().then(() => {
        res.redirect('/managecategory')
    }).catch((err) => {
        req.flash('errors', err)
        console.log(err)
        req.session.save(function () {
            res.redirect("/admin");
        });
    });
}


exports.updateCategory = function (req, res) {
    let category = new Category(req.body);
    category.UpdateCategory().then((response) => {
        req.session.admin.categorydetail = null;
        req.session.save(() => {
            res.redirect('/managecategory')
        })
    }).catch((err) => {
        req.flash('errors', err)
        console.log(err)
        req.session.save(function () {
            res.redirect("/admin");
        });
    });
}


exports.deleteCategory = function (req, res) {
    let category = new Category(req.params);
    category.DeleteCategory().then(() => {
        req.session.admin.category = null;
        req.session.save(() => {
            res.redirect('/managecategory')
        })
    }).catch((err) => {
        req.flash('errors', err)
        console.log(err)
        req.session.save(function () {
            res.redirect("/managecategory");
        });
    });
}

exports.getCategoryList = function (req, res, next) {
    let category = new Category();
    category.getCategoryList().then((categorylist) => {
        console.log(categorylist)
        if (categorylist.length != 0) {
            req.session.categorylist = categorylist;
        } else {
            req.session.categorylist = null;
        }
        req.session.save()
        next()
    });
};


exports.getCategoryDetail = function (req, res, next) {
    let categorydetail = new Category(req.params)
    categorydetail.CategoryDetail().then((categorydetail) => {
        req.session.admin.categorydetail = categorydetail;
        req.session.save(() => {
            res.redirect('/managecategory')
        })
        console.log(req.session.admin)
    }).catch((err) => {
        req.flash('errors', err)
        console.log(err)

    });

}


/* ---------------------------------------------Manage video section------------------------------------- */



exports.manageVideosPage = function (req, res) {
    res.render('admin-managevideos', {
        videolist: req.session.videolist,
        videodetail: req.session.admin.videodetail
    })
}


exports.addVideo = function (req, res) {
    let video = new Video(req.body);
    video.AddVideo().then(() => {
        res.redirect('/managevideo')
    }).catch((err) => {
        req.flash('errors', err)
        console.log(err)
        req.session.save(function () {
            res.redirect("/admin");
        });
    });
}


exports.updateVideo = function (req, res) {
    let video = new Video(req.body);
    video.UpdateVideo().then((response) => {
        req.session.admin.videodetail = null;
        req.session.save(() => {
            res.redirect('/managevideo')
        })
    }).catch((err) => {
        req.flash('errors', err)
        console.log(err)
        req.session.save(function () {
            res.redirect("/admin");
        });
    });
}


exports.deleteVideo = function (req, res) {
    let video = new Video(req.params);
    video.DeleteVideo().then(() => {
        req.session.admin.video = null;
        req.session.save(() => {
            res.redirect('/managevideo')
        })
    }).catch((err) => {
        req.flash('errors', err)
        console.log(err)
        req.session.save(function () {
            res.redirect("/managevideo");
        });
    });
}

exports.getVideoList = function (req, res, next) {
    let video = new Video();
    video.getVideoList().then((videolist) => {
        console.log(videolist)
        if (videolist.length != 0) {
            req.session.videolist = videolist;
        } else {
            req.session.videolist = null;
        }
        req.session.save()
        next()
    });
};


exports.getVideoDetail = function (req, res, next) {
    let videodetail = new Video(req.params)
    videodetail.VideoDetail().then((videodetail) => {
        req.session.admin.videodetail = videodetail;
        req.session.save(() => {
            res.redirect('/managevideo')
        })
        console.log(req.session.admin)
    }).catch((err) => {
        req.flash('errors', err)
        console.log(err)
        req.session.save(function () {
            res.redirect("/managevideo");
        });
    });
}


/* ---------------------------------------------Admin authentication section------------------------------------- */


exports.getAdminPage = function (req, res) {
    res.render('admin-signin')
};



exports.adminSignin = (req, res) => {
    let admin = new Admin(req.body)
    admin.login().then((admin) => {
        console.log('customer ' + admin);
        req.session.admin = {
            adminid: admin.adminid,
            email: admin.email,
            isadmin: true
        };
        req.session.save(function () {
            res.redirect('/admin')
        });
    }).catch((err) => {
        req.flash('errors', err)
        console.log(err)
        req.session.save(function () {
            res.redirect("/admin-signin");
        });
    });
};


// signout post route and it will local cookies
exports.adminSignout = (req, res) => {
    req.session.destroy(function () {
        res.redirect('/');
    });
};


// middleware for  Login check to access the page 
exports.mustBeLoggedIn = function (req, res, next) {
    if (req.session.admin) {
        console.log('admin session', req.session.admin)
        next();
    } else {
        req.flash('errors', "user must be logged into access this page");
        req.session.save(function () {
            res.redirect("/admin-signin");
        });
    }
};