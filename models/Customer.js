const bcrypt = require("bcrypt")
const customerdb = require('../models/db/Customersdb');
const OTPdb = require('../models/db/OTPdb');
const sgMail = require('@sendgrid/mail');
const customer = require("../models/db/Customersdb");
require('dotenv').config()
sgMail.setApiKey(process.env.SENDGRID_SECRETE_KEY);
const apiController = require('../controllers/apiController');
const { response } = require("express");



let Customer = function (data) {
    this.data = data;
    this.error = [];
}

Customer.prototype.cleanUp = function () {
    if (typeof (this.data.name) != "string") { this.data.name = "" }
    if (typeof (this.data.email) != "string") { this.data.email = "" }
    if (typeof (this.data.password) != "string") { this.data.password = "" }
    if (typeof (this.data.conformpassword) != "string") { this.data.conformpassword = "" }
    if (typeof (this.data.phone) != "string") { this.data.phone = "" }


    this.data = {
        name: this.data.name.trim(),
        email: this.data.email.trim().toLowerCase(),
        phone: this.data.phone.trim(),
        password: this.data.password,
        conformpassword: this.data.conformpassword,
        addressline1: this.data.addressline1,
        addressline2: this.data.addressline2,
        city: this.data.city,
        state: this.data.state,
        pincode: this.data.pincode,
        customerid: this.data.customerid
    }
    console.log(this.data)
}

Customer.prototype.validate = function () {

    return new Promise(async (resolve, reject) => {
        console.log(this.data.password)
        console.log(this.data.conformpassword)
        if (this.data.email == "") { this.error.push("Email should not be empty") }
        if (this.data.password != "") {
            if (this.data.password === this.data.conformpassword) {
                if (this.data.password.length < 5 || this.data.password.length > 12) { this.error.push("Password should be in length between 5 to 12") }
            } else {
                this.error.push("Password does not match")
            }
        } else { this.error.push("Password should not be empty") }

        if (this.data.phone.length < 10 || this.data.password.length > 12) { this.error.push("Enter valid phone number") }

        let emailExists = await customerdb.findOne({ where: { email: this.data.email } })
        console.log(emailExists)
        if (emailExists) { this.error.push("That email is already being used") }
        resolve()
    })
}



Customer.prototype.signup = function () {
    return new Promise(async (resolve, reject) => {
        console.log(this.data)
        console.log(typeof (this.data.name))
        console.log(typeof (this.data.email))
        console.log(typeof (this.data.password))
        console.log(typeof (this.data.conformpassword))
        this.cleanUp()
        await this.validate()
        if (!this.error.length) {
            const password = await bcrypt.hash(this.data.password, 10);
            customerdb.create({
                name: this.data.name,
                email: this.data.email,
                password: password,
                phone: this.data.phone
            }).then((customer) => resolve(customer)).catch(err => console.log(err))
        } else {
            reject(this.error)
        }
    })
}




Customer.prototype.login = function () {
    return new Promise((resolve, reject) => {
        this.cleanUp()
        // if (attemptedUser && attemptedUser.password == this.data.password) 
        customerdb.findOne({ where: { email: this.data.email } }).then(attemptedcustomer => {
            if (attemptedcustomer && bcrypt.compareSync(this.data.password, attemptedcustomer.password)) {
                resolve(attemptedcustomer)
            } else {
                reject("Invalid email / password")
            }
        }).catch(function () {
            reject("Please try again later")
        })
    })
}


function getRandomNumber(min, max) {
    return Math.ceil(Math.random() * (max - min) + min);
}


Customer.prototype.sendOTP = function () {
    return new Promise((resolve, reject) => {
        this.cleanUp();
        customerdb.findOne({ where: { email: this.data.email } }).then((isattemptted) => {
            if (isattemptted) {
                let otp = getRandomNumber(1000, 9999)
                const msg = {
                    to: this.data.email,
                    from: 'contact@thewildmart.com',
                    subject: 'The WildMart - Login OTP',

                    //text: "I'm sending myself an email " + otp
                    html: `<center><h1>Welcome to The WildMart</h1></center><br><h3>Your OTP is</h3>${otp}`
                }
                apiController.sendEmail(msg).then(mailresponse => {
                    OTPdb.create({
                        email: this.data.email,
                        otp: otp
                    }).then((record) => {
                        resolve(record)
                    }).catch(err => console.log(err))
                    console.log('Message sent')
                }).catch((error) => {
                    console.log(error.response.body)
                })
            } else {
                console.log('Email not available')
                reject('Email not available')
            }
        }).catch(err => console.log(err))
    })
}

Customer.prototype.validateOTP = function () {
    return new Promise((resolve, reject) => {
        OTPdb.findByPk(this.data.email).then((isattempted) => {
            console.log(isattempted)
            if (isattempted.otp == this.data.otp) {
                OTPdb.destroy({
                    where: {
                        email: isattempted.email
                    },
                    force: true
                }).then(() => {
                    resolve(customerdb.findOne({ where: { email: this.data.email } }))
                }).catch(err => reject(err))
            } else {
                console.log('invalid otp')
                reject('Invalid OTP')
            }
        });
    });
}


Customer.prototype.UpdateCustomer = function () {
    return new Promise(async (resolve, reject) => {
        customerdb.update({
            addressline1: this.data.addressline1.trim(),
            addressline2: this.data.addressline2.trim(),
            city: this.data.city.trim(),
            state: this.data.state.trim(),
            pincode: this.data.pincode,
            name: this.data.name,
            email: this.data.email,
            phone: this.data.phone
        }, {
            where: {
                customerid: this.data.customerid
            }
        }).then(customer => {
            if (customer != 0) {
                customerdb.findByPk(this.data.customerid).then(cusdata => resolve(cusdata))
            } else {
                reject('Error while updating customer data')
            }
        })
            .catch(err => reject('Error while updating customer data'))
    })
}


module.exports = Customer