const { Sequelize, DataTypes } = require('sequelize')
const db = require('../../utils/db');

const customer = db.define('Customers', {
    customerid: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name: DataTypes.STRING,
    email: {
        type: DataTypes.STRING,
        validate: {
            isEmail: true
        },
        unique: true
    },
    phone: {
        type: DataTypes.BIGINT,
        validate: {
            len: [10, 12]
        }
    },
    password: DataTypes.STRING,
    addressline1: DataTypes.STRING,
    addressline2: DataTypes.STRING,
    city: DataTypes.STRING,
    state: DataTypes.STRING,
    pincode: {
        type: DataTypes.BIGINT,
        validate: {
            len: [5, 8]
        }
    }
}, {
    initialAutoIncrement: '50000'
})

module.exports = customer;