const Videodb = require('../models/db/Videosdb')


let Video = function (data) {
    this.data = data;
    this.error = [];
}

Video.prototype.cleanUp = function () {
    if (typeof (this.data.title) != "string") { this.data.title = "" }
    if (typeof (this.data.description) != "string") { this.data.description = "" }
    if (typeof (this.data.link) != "string") { this.data.link = "" }

    this.data = {
        videoid: this.data.videoid,
        title: this.data.title.trim(),
        description: this.data.description.trim(),
        link: this.data.link
    }
    console.log(this.data)
}


Video.prototype.AddVideo = function () {
    return new Promise(async (resolve, reject) => {
        console.log(this.data)
        this.cleanUp()
        if (!this.error.length) {
            Videodb.create({
                title: this.data.title,
                description: this.data.description,
                link: this.data.link
            }).then((Video) => resolve()).catch(err => console.log(err))
        } else {
            reject(this.error)
        }
    })
}


Video.prototype.UpdateVideo = function () {
    return new Promise(async (resolve, reject) => {
        console.log(this.data)
        this.cleanUp()
        if (!this.error.length) {
            Videodb.update({
                title: this.data.title,
                description: this.data.description,
                link: this.data.link
            },
                {
                    where: {
                        videoid: this.data.videoid
                    }
                }).then((Video) => resolve(Video))
                .catch(err => console.log(err))
        } else {
            reject(this.error)
        }
    })
}


Video.prototype.DeleteVideo = function () {
    return new Promise(async (resolve, reject) => {
        console.log(this.data)
        Videodb.destroy({
            where: {
                videoid: this.data.videoid
            },
            force: true
        }).then(() => resolve()).catch((err) => reject(err))
    })
}



Video.prototype.getVideoList = function () {
    return new Promise(async (resolve, reject) => {
        Videodb.findAll().then(videolist => resolve(videolist))
            .catch(err => reject(err))
    })
}


Video.prototype.VideoDetail = function () {
    return new Promise(async (resolve, reject) => {
        Videodb.findByPk(this.data.videoid)
            .then(videodetail => resolve(videodetail))
            .catch(err => reject(err))
    })
}


module.exports = Video