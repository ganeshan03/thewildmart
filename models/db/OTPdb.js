const { Sequelize, DataTypes } = require('sequelize')
const db = require('../../utils/db')

const otpdb = db.define('authotps', {
    email: {
        type: DataTypes.STRING,
        primaryKey: true
    },
    otp: DataTypes.BIGINT
})

module.exports = otpdb;