const bcrypt = require("bcrypt")
const admindb = require('../models/db/Admindb');
const sgMail = require('@sendgrid/mail');
require('dotenv').config()
sgMail.setApiKey(process.env.SENDGRID_SECRETE_KEY);



let Admin = function (data) {
    this.data = data;
    this.error = [];
}

Admin.prototype.cleanUp = function () {
    if (typeof (this.data.email) != "string") { this.data.email = "" }
    if (typeof (this.data.password) != "string") { this.data.password = "" }


    this.data = {
        email: this.data.email.trim().toLowerCase(),
        password: this.data.password,
    }
    console.log(this.data)
}


Admin.prototype.validate = function () {

    return new Promise(async (resolve, reject) => {
        if (this.data.email == "") { this.error.push("Email should not be empty") }
        if (this.data.password != "") {
            if (this.data.password === this.data.conformpassword) {
                if (this.data.password.length < 5 || this.data.password.length > 12) { this.error.push("Password should be in length between 5 to 12") }
            } else {
                this.error.push("Password does not match")
            }
        } else { this.error.push("Password should not be empty") }

        let emailExists = await admindb.findOne({ where: { email: this.data.email } })
        console.log(emailExists)
        if (emailExists) { this.error.push("That email is already being used") }
        resolve()
    })
}



Admin.prototype.login = function () {
    return new Promise((resolve, reject) => {
        this.cleanUp()
        // if (attemptedUser && attemptedUser.password == this.data.password) 
        admindb.findOne({ where: { email: this.data.email } }).then(attemptedcustomer => {
            if (attemptedcustomer && bcrypt.compareSync(this.data.password, attemptedcustomer.password)) {
                resolve(attemptedcustomer)
            } else {
                reject("Invalid email / password")
            }
        }).catch(function () {
            reject("Please try again later")
        })
    })
}


module.exports = Admin