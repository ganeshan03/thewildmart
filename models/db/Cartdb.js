const { Sequelize, DataTypes } = require('sequelize');
const db = require('../../utils/db')

const cart = db.define('Carts', {
    cartid: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    customerid: DataTypes.INTEGER,
    productid: DataTypes.INTEGER,
    productname: DataTypes.STRING,
    imageurl: DataTypes.STRING,
    quantity: DataTypes.INTEGER,
    quantityoption: DataTypes.STRING,
    totalprice: DataTypes.DECIMAL
}, {
    initialAutoIncrement: '10000'
})

module.exports = cart;