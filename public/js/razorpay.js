// document.getElementById('rbutton').onclick = async function (e) {
//     var amt = parseFloat(document.getElementById('amount').value);
//     console.log("amount " + amt);
//     const backend_call = await fetch('http://localhost:3000/razorpay', {
//         headers: {
//             "Content-Type": "application/json"
//         },
//         method: "POST",
//         body: JSON.stringify({ amount: amt })
//     }).then(async (backend_value) => {
//         var data = await backend_value.json()
//         console.log(data)
//         var options = {
//             "key": "rzp_test_Pg1UiEbbmK1wvh",
//             "amount": data.amount,
//             "currency": data.currency,
//             "name": "The Wildmart",
//             "description": "Customer Transaction",
//             "image": "/public/images/logowild.png",
//             "order_id": data.orderid,
//             "handler": async function (response) {
//                 console.log(response);
//                 const verify_call = await fetch('http://localhost:3000/razorpay/verify', {
//                     headers: {
//                         "Content-Type": "application/json"
//                     },
//                     method: "POST",
//                     body: JSON.stringify({
//                         razorpay_payment_id: response.razorpay_payment_id,
//                         razorpay_order_id: response.razorpay_order_id,
//                         razorpay_signature: response.razorpay_signature
//                     })
//                 }).then((verify_back) => {
//                     console.log(verify_back.status)
//                     if (verify_back.status == 200) {
//                         alert("payment is completed successfully")
//                     } else {
//                         alert("payment isn't successfully completed")
//                     }
//                 })
//             },
//             "prefill": {
//                 "name": "ganeshan",
//                 "email": "ganeshannt@gmail.com",
//                 "contact": "8124348247"
//             }
//         };
//         var rzp1 = new Razorpay(options);
//         rzp1.open();
//         e.preventDefault();
//     }).catch(err => console.log(err))

// }




document.getElementById('rbutton').onclick = async function (e) {
    const backend_call = await fetch('http://localhost:3000/placeorder', {
        headers: {
            "Content-Type": "application/json"
        },
        method: "POST",
        redirect: "follow",
        body: JSON.stringify(
            {
                totalprice: parseFloat(document.getElementById('amount').value),
                firstname: document.getElementById('firstname').value,
                lastname: document.getElementById('lastname').value,
                email: document.getElementById('email').value,
                phone: document.getElementById('phone').value,
                addressline1: document.getElementById('addressline1').value,
                addressline2: document.getElementById('addressline2').value,
                city: document.getElementById('city').value,
                state: document.getElementById('state').value,
                pincode: document.getElementById('pincode').value,
                shippingprice: parseFloat(document.getElementById('shippingcharge').value)
            })
    }).then(async (backend_value) => {
        var data = await backend_value.json()
        console.log(data)
        var options = {
            "key": "rzp_test_Pg1UiEbbmK1wvh",
            "amount": data.amount,
            "currency": data.currency,
            "name": "The Wildmart",
            "description": "Customer Transaction",
            "image": "/public/images/logowild.png",
            "order_id": data.orderid,
            "handler": async function (response) {
                console.log(response);
                const verify_call = await fetch('http://localhost:3000/razorpay/verify', {
                    headers: {
                        "Content-Type": "application/json"
                    },
                    method: "POST",
                    redirect: "follow",
                    body: JSON.stringify({
                        razorpay_payment_id: response.razorpay_payment_id,
                        razorpay_order_id: response.razorpay_order_id,
                        razorpay_signature: response.razorpay_signature
                    })
                }).then(async (verify_back) => {
                    console.log(verify_back.status)
                    if (verify_back.status == 200) {
                        console.log("payment is completed successfully")


                        const postpay = await fetch('http://localhost:3000/postpayment', {
                            headers: {
                                "Content-Type": "application/json"
                            },
                            method: "POST",
                            redirect: "follow",
                            body: JSON.stringify({
                                orderid: data.product_orderid,
                                razorpay_payment_id: response.razorpay_payment_id,
                                razorpay_order_id: response.razorpay_order_id,
                                razorpay_signature: response.razorpay_signature
                            }),

                        })
                        // .then(res => {
                        //     console.log("kdfjgnfd", res)
                        //     if (res.status == 200) {
                        //         const orderdonepage = fetch('http://localhost:3000/checkout/done', {
                        //             headers: {
                        //                 "Content-Type": "application/json"
                        //             },
                        //             method: "GET",
                        //         })
                        //     } else {
                        //         alert("error")
                        //     }
                        // })
                    } else {
                        console.log("payment isn't successfully completed")
                    }
                })
            },
            "prefill": {
                "name": data.fullname,
                "email": data.email,
                "contact": data.phone
            }
        };
        var rzp1 = new Razorpay(options);
        rzp1.open();
        e.preventDefault();
    }).catch(err => console.log(err))
}