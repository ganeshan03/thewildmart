const { Sequelize, DataTypes } = require('sequelize')
const db = require('../../utils/db');

const admin = db.define('Admins', {
    adminid: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name: DataTypes.STRING,
    email: {
        type: DataTypes.STRING,
        validate: {
            isEmail: true
        },
        unique: true
    },
    password: DataTypes.STRING
})

module.exports = admin;